package snakegame.grid;

import java.util.ArrayList;
import java.util.Iterator;

public class Grid<E> implements IGrid<E> {

    ArrayList<E> grid;
    int rows;
    int cols;
    E element;

    public Grid(int rows, int cols) {
        this(rows, cols, null);
    }

    public Grid(int rows, int cols, E element) {
        grid = new ArrayList<>();
        this.rows = rows;
        this.cols = cols;
        this.element = element;

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                grid.add(element);
            }
        }
    }

    @Override
    public int getRows() {
        return rows;
    }

    @Override
    public int getCols() {
        return cols;
    }

    @Override
    public void set(Coordinate coordinate, E item) {
        if (!coordinateIsOnGrid(coordinate)) {
            throw new IndexOutOfBoundsException();
        }
        grid.set(indexOf(coordinate.row, coordinate.col), item);
    }

    @Override
    public E get(Coordinate coordinate) {
        if (!coordinateIsOnGrid(coordinate)) {
            throw new IndexOutOfBoundsException();
        }
        return grid.get(indexOf(coordinate.row, coordinate.col));
    }

    @Override
    public boolean coordinateIsOnGrid(Coordinate coordinate) {
        return coordinate.row < rows && coordinate.row >= 0 && coordinate.col < cols && coordinate.col >= 0;
    }

    @Override
    public Iterator<CoordinateItem<E>> iterator() {
        ArrayList<CoordinateItem<E>> list = new ArrayList<>();
        for (int row = 0; row < rows; row++) {
            for (int col = 0; col < cols; col++) {
                CoordinateItem<E> item = new CoordinateItem<>(new Coordinate(row, col), grid.get(indexOf(row, col)));
                list.add(item);
            }
        }
        return list.iterator();
    }

    private Integer indexOf(int row, int col) {
        return col + row * cols;
    }

}
