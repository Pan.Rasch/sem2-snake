package snakegame.grid;

import java.util.Objects;

public class Coordinate {

    public final int row;
    public final int col;

    public Coordinate(int row,int col) {
        this.row = row;
        this.col = col;
    }

    @Override
    public String toString() {
        return "{ " +
                "row='" + row + "'" +
                ", col='" + col + "'" +
                " }";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Coordinate that = (Coordinate) o;
        return row == that.row && col == that.col;
    }

    @Override
    public int hashCode() {
        return Objects.hash(row, col);
    }
}
