package snakegame.snake.model;

import snakegame.grid.Coordinate;
import snakegame.grid.CoordinateItem;
import snakegame.grid.Grid;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;

public class Fruit implements Iterable<CoordinateItem<Tile>> {

    Random r;
    CoordinateItem<Tile> fruit;
    ArrayList<Integer> excludeRows;
    ArrayList<Integer> excludeCols;
    int row;
    int col;

    // For testing purposes
    public Fruit(Coordinate coordinate) {
        fruit = new CoordinateItem<>(coordinate,new Tile(null,2));
    }

    public Fruit(Model model) {
        excludeRows = new ArrayList<>();
        excludeCols = new ArrayList<>();
        r = new Random();

        // Exclude all coordinates with snake on them
        for(CoordinateItem<Tile> tile : model.snake) {
            excludeRows.add(tile.coordinate.row);
            excludeCols.add(tile.coordinate.col);
        }
        for(int i = 0; i < excludeRows.size(); i++) {
            row = r.nextInt(model.getRows());
            if(!excludeRows.contains(row)) { break; }
        }
        for(int j = 0; j < excludeCols.size(); j++) {
            col = r.nextInt(model.getCols());
            if(!excludeCols.contains(col)) { break; }
        }

        fruit = new CoordinateItem<>(new Coordinate(row, col), new Tile(null,2));
    }

    @Override
    public Iterator<CoordinateItem<Tile>> iterator() {
        ArrayList<CoordinateItem<Tile>> it = new ArrayList<>();
        it.add(fruit);
        return it.iterator();
    }
}
