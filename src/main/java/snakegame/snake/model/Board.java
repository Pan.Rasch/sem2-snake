package snakegame.snake.model;

import snakegame.grid.CoordinateItem;
import snakegame.grid.Grid;

public class Board extends Grid<Tile> {

    public Board(int row,int col,Tile tile) {
        super(row,col,tile);
    }

    /**
     * Loops through coordinates on board and creates
     * 2d array of chars based on the number on each Tile.
     * @return 2d array of chars
     */
    public char[][] toCharArray2d() {
        char[][] charArray2d = new char[getRows()][getCols()];
        for(CoordinateItem<Tile> cord : this) {
            if(get(cord.coordinate) != null) {
                charArray2d[cord.coordinate.row][cord.coordinate.col] = 'o';
            } else charArray2d[cord.coordinate.row][cord.coordinate.col] = '-';
        }
        return charArray2d;
    }

    /**
     * Loops through param charArray and creates StringBuilder
     * that stores char values in one String.
     * @param charArray to convert to String
     * @return String representing param charArray
     */
    public String charArray2dToString(char[][] charArray) {
        StringBuilder aString = new StringBuilder();
        for (int row = 0; row < charArray.length; row++) {
            for (int col = 0; col < charArray[0].length; col++) {
                aString.append(charArray[row][col]);
            }
            aString.append("\n");
        }
        return aString.toString();
    }

}
