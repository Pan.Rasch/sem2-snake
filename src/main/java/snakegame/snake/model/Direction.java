package snakegame.snake.model;

public enum Direction {

    NORTH(-1,0),
    EAST(0,1),
    SOUTH(1,0),
    WEST(0,-1);

    final int deltaRow;
    final int deltaCol;

    Direction(int deltaRow,int deltaCol) {
        this.deltaRow = deltaRow;
        this.deltaCol = deltaCol;
    }

}
