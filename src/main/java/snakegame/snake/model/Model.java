package snakegame.snake.model;

import snakegame.grid.Coordinate;
import snakegame.grid.CoordinateItem;
import snakegame.snake.controller.SnakeControllable;
import snakegame.snake.view.SnakeViewable;

public class Model implements SnakeViewable, SnakeControllable {

    Board board;
    GameState state;
    Direction direction;
    Snake snake;
    Fruit fruit;
    int fruitCount;

    // For testing purposes
    public Model(Snake snake, Coordinate coordinate) {
        board = new Board(5,10,new Tile(null,0));
        this.snake = snake;
        state = GameState.ACTIVE;
        fruit = new Fruit(coordinate);
        direction = Direction.EAST;
    }

    public Model() {
        state = GameState.WELCOME;
        board = new Board(20,20,new Tile(null,0));
        snake = new Snake();
        fruit = new Fruit(this);
        fruitCount = 0;
        direction = Direction.EAST;
    }

    @Override
    public int getRows() {
        return board.getRows();
    }

    @Override
    public int getCols() {
        return board.getCols();
    }

    @Override
    public int getScore() { return fruitCount; }

    @Override
    public Iterable<CoordinateItem<Tile>> boardCords() {
        return board;
    }

    @Override
    public Iterable<CoordinateItem<Tile>> snakeCords() {
        return snake;
    }

    @Override
    public Iterable<CoordinateItem<Tile>> fruitCords() {
        return fruit;
    }

    @Override
    public GameState getGameState() {
        return state;
    }

    @Override
    public void setGameState(GameState state) {
        this.state = state;
    }

    @Override
    public int timeBetweenTicks() {
        return 85;
    }

    @Override
    public void clockTick() {
        if(!moveSnake()) {
            setGameState(GameState.GAME_OVER);
        }
        eatFruit();
    }

    @Override
    public int calculateTimerDelay() {
        return (int) (timeBetweenTicks()*(Math.pow(0.99,fruitCount)));
    }

    @Override
    public void changeDirection(Direction direction) {
        this.direction = direction;
    }

    @Override
    public void restartGame() {
        board = new Board(getRows(),getCols(),new Tile(null,0));
        snake = new Snake();
        fruit = new Fruit(this);
        fruitCount = 0;
        direction = Direction.EAST;
        setGameState(GameState.ACTIVE);
    }

    @Override
    public Direction getDirection() {
        return direction;
    }

    // Move snake one tile in active direction if said tile is on board and not on snake.
    private boolean moveSnake() {
        if(isOnBoard()) {
            if(!isOnSnake(
                    snake.snake.get(0).coordinate.row+direction.deltaRow,
                    snake.snake.get(0).coordinate.col+direction.deltaCol)
            ) {
                snake.updateSnake(direction.deltaRow, direction.deltaCol,false);
                return true;
            } else return false;
        } else return false;
    }

    // Attempt to eat a fruit, if yes spawn new fruit
    private void eatFruit() {
        if(snake.snake.get(0).coordinate.equals(fruit.fruit.coordinate)) {
            snake.updateSnake(0,0,true);
            fruitCount++;
            fruit = new Fruit(this);
        }
    }

    // Check if next location of snakes head is on the board
    private boolean isOnBoard() {
        return board.coordinateIsOnGrid(new Coordinate(
                snake.snake.get(0).coordinate.row + direction.deltaRow,
                snake.snake.get(0).coordinate.col + direction.deltaCol));
    }

    // Check if given coordinate is on snake
    private boolean isOnSnake(int row,int col) {
        for(CoordinateItem<Tile> tile : snake) {
            if(tile.coordinate.row == row && tile.coordinate.col == col) {
                return true;
            }
        } return false;
    }

    /**
     * Iterates over tiles on board and creates a 2d
     * char array representing the values of the tiles.
     * '-' if tile number is 0, else 'o'.
     * @return 2d char array
     */
     char[][] boardToCharArray() {
        char[][] charArray2d = new char[getRows()][getCols()];
        for(CoordinateItem<Tile> tile : snake) {
            board.set(tile.coordinate,tile.item);
        }
        for(CoordinateItem<Tile> cord : board) {
            if(cord.item.number != 0) {
                charArray2d[cord.coordinate.row][cord.coordinate.col] = 'o';
            } else charArray2d[cord.coordinate.row][cord.coordinate.col] = '-';
        }
        return charArray2d;
    }
}
