package snakegame.snake.model;

public enum GameState {

    ACTIVE,
    GAME_OVER,
    WELCOME,
    PAUSED

}
