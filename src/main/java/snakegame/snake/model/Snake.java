package snakegame.snake.model;

import snakegame.grid.Coordinate;
import snakegame.grid.CoordinateItem;

import java.util.ArrayList;
import java.util.Iterator;

public class Snake implements Iterable<CoordinateItem<Tile>> {

    ArrayList<CoordinateItem<Tile>> snake;

    // For testing purposes
    public Snake(int test) {
        snake = new ArrayList<>();
        snake.add(new CoordinateItem<>(new Coordinate(4,7),new Tile(null,1)));
        snake.add(new CoordinateItem<>(new Coordinate(4,6),new Tile(null,1)));
        snake.add(new CoordinateItem<>(new Coordinate(4,5),new Tile(null,1)));
        snake.add(new CoordinateItem<>(new Coordinate(4,4),new Tile(null,1)));
        snake.add(new CoordinateItem<>(new Coordinate(4,3),new Tile(null,1)));


    }

    public Snake() {
        snake = new ArrayList<>();
        snake.add(new CoordinateItem<>(new Coordinate(9,5),new Tile(null,1)));
        snake.add(new CoordinateItem<>(new Coordinate(9,4),new Tile(null,1)));
        snake.add(new CoordinateItem<>(new Coordinate(9,3),new Tile(null,1)));
        snake.add(new CoordinateItem<>(new Coordinate(9,2),new Tile(null,1)));
    }

    @Override
    public Iterator<CoordinateItem<Tile>> iterator() {
        return snake.iterator();
    }

    // Creates new snake with head moved one tile in active direction, rest of snake body follows
    void updateSnake(int deltaRow, int deltaCol, boolean growth) {
        CoordinateItem<Tile> currentHead = snake.get(0);
        CoordinateItem<Tile> newHead = new CoordinateItem<>(
                        new Coordinate(currentHead.coordinate.row+deltaRow, currentHead.coordinate.col+deltaCol),
                        new Tile(null,1));
        ArrayList<CoordinateItem<Tile>> newSnake = new ArrayList<>();
        newSnake.add(newHead);

        for(int i = 0; i < snake.size()-1; i++) {
            newSnake.add(snake.get(i));
        }

        if(growth) { newSnake.add(snake.get(snake.size()-1)); }
        snake = newSnake;
    }
}
