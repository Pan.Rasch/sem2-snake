package snakegame.snake.model;

import java.awt.*;

public class Tile {

    public Color color; // green1/2 is board. blue is snake. red is fruit.
    public int number; // 0 is board. 1 is snake. 2 is fruit.

    public Tile(Color color,int number) {
        this.color = color;
        this.number = number;
    }
}
