package snakegame.snake.controller;

import snakegame.snake.model.GameState;
import snakegame.snake.model.Direction;

public interface SnakeControllable {

    /** returns current game state */
    GameState getGameState();

    /**
     * Sets game state to given state
     * @param gameState to set state to
     */
    void setGameState(GameState gameState);

    /** @return time between each tick, in milliseconds */
    int timeBetweenTicks();

    /**
     * One clock tick consists of a move in active direction.
     * Updates snake location and attempts to eat fruit.
     * If snake makes illegal move, set game state to game over.
     */
    void clockTick();

    /**
     * Calculates delay between this tick and the next,
     * less and less with each fruit eaten.
     * @return time between this tick and next tick
     */
    int calculateTimerDelay();

    /**
     * Changes direction of the snake
     * (north, east, south, west)
     * @param dir given direction
     */
    void changeDirection(Direction dir);

    /**
     * Clears the board of all entities, spawns new snake and new fruit.
     * Resets score and direction.
     */
    void restartGame();

    /** @return current direction snake is moving in */
    Direction getDirection();
}
