package snakegame.snake.controller;


import snakegame.snake.model.Direction;
import snakegame.snake.model.GameState;
import snakegame.snake.view.SnakeView;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.security.Key;
import java.text.AttributedCharacterIterator;
import javax.swing.*;

public class SnakeController implements KeyListener,ActionListener {

    Timer timer;
    SnakeControllable controllable;
    SnakeView view;

    public SnakeController(SnakeControllable controllable, SnakeView view) {
        this.controllable = controllable;
        this.view = view;
        view.addKeyListener(this);
        this.timer = new Timer(controllable.timeBetweenTicks(),this);
        timer.start();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(controllable.getGameState() == GameState.ACTIVE) {
            controllable.clockTick();
            view.repaint();
            updateDelays();
        }
    }

    @Override
    public void keyTyped(KeyEvent e) {}

    @Override
    public void keyPressed(KeyEvent e) {
        if(controllable.getGameState() == GameState.WELCOME) {
            if(e.getKeyCode() == KeyEvent.VK_SPACE) {
                controllable.setGameState(GameState.ACTIVE);
            }
        }

        if(controllable.getGameState() == GameState.ACTIVE) {
            if (e.getKeyCode() == KeyEvent.VK_UP && controllable.getDirection() != Direction.SOUTH && controllable.getDirection() != Direction.NORTH) {
                controllable.changeDirection(Direction.NORTH);
                controllable.clockTick();
            }
            if(e.getKeyCode() == KeyEvent.VK_RIGHT && controllable.getDirection() != Direction.WEST && controllable.getDirection() != Direction.EAST) {
                controllable.changeDirection(Direction.EAST);
                controllable.clockTick();
            }
            if(e.getKeyCode() == KeyEvent.VK_DOWN && controllable.getDirection() != Direction.NORTH && controllable.getDirection() != Direction.SOUTH) {
                controllable.changeDirection(Direction.SOUTH);
                controllable.clockTick();
            }
            if(e.getKeyCode() == KeyEvent.VK_LEFT && controllable.getDirection() != Direction.EAST && controllable.getDirection() != Direction.WEST) {
                controllable.changeDirection(Direction.WEST);
                controllable.clockTick();
            }
        }

        if(e.getKeyCode() == KeyEvent.VK_ESCAPE) {
            if(controllable.getGameState() == GameState.ACTIVE) {
                controllable.setGameState(GameState.PAUSED);
            } else if(controllable.getGameState() == GameState.PAUSED) {
                controllable.setGameState(GameState.ACTIVE);
            }
        }

        if(controllable.getGameState() == GameState.PAUSED) {
            if(e.getKeyCode() == KeyEvent.VK_R) {
                controllable.restartGame();
            }
        }

        if(controllable.getGameState() == GameState.GAME_OVER) {
            if(e.getKeyCode() == KeyEvent.VK_SPACE) {
                controllable.restartGame();
            }
        }

        view.repaint();
    }

    @Override
    public void keyReleased(KeyEvent e) {}

    private void updateDelays() {
        int delay = controllable.calculateTimerDelay();
        timer.setDelay(delay);
        timer.setInitialDelay(delay);
    }
}
