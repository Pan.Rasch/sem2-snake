package snakegame.snake;

import snakegame.snake.controller.SnakeController;
import snakegame.snake.model.Model;
import snakegame.snake.view.SnakeView;

import javax.swing.*;

public class Main {
    public static final String WINDOW_TITLE = "Snake";

    public static void main(String[] args) {
        Model model = new Model();
        SnakeView view = new SnakeView(model);
        SnakeController controller = new SnakeController(model,view);

        JFrame frame = new JFrame(WINDOW_TITLE);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        frame.setContentPane(view);

        frame.pack();
        frame.setVisible(true);
    }

}
