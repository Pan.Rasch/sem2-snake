package snakegame.snake.view;

import snakegame.grid.CoordinateItem;
import snakegame.snake.model.GameState;
import snakegame.snake.model.Tile;

import javax.swing.*;
import java.awt.*;

public class SnakeView extends JComponent {

    {
        this.setFocusable(true);
    }

    SnakeViewable viewable;
    int score;
    Font welcomeFont = new Font("SansSerif",Font.BOLD,60);
    Font subFont = new Font("SansSerif",Font.ITALIC,30);

    public SnakeView(SnakeViewable viewable) {
        this.viewable = viewable;
    }

    @Override
    public void paint(Graphics canvas) {
        super.paint(canvas);
        drawSnakeBoard(canvas,getX(),getY(),getWidth(),getHeight(),viewable.boardCords());
        drawSnakeBoard(canvas,getX(),getY(),getWidth(),getHeight(),viewable.snakeCords());
        drawSnakeBoard(canvas,getX(),getY(),getWidth(),getHeight(),viewable.fruitCords());
        drawWelcome(canvas);
        drawPause(canvas);
        drawGameOver(canvas);
        drawScore(canvas);
    }

    /**
     * Draws Tile based on parameters given.
     * @param g to paint on
     * @param x x value for Tile
     * @param y y value for Tile
     * @param width width of Tile
     * @param height height of Tile
     * @param color color of Tile
     */
    private void drawTile(Graphics g, int x, int y, int width, int height, Color color) {
        g.setColor(color);
        g.fillRect(x,y,width,height);
    }

    private void drawFruit(Graphics g, int x, int y, int width, int height, Color color) {
        g.setColor(color);
        g.fillOval(x,y,width,height);
    }

    private void drawScore(Graphics g) {
        score = viewable.getScore()*100;
        g.setColor(Color.BLACK);
        Font myFont = new Font("SansSerif", Font.ITALIC,13);
        g.setFont(myFont);
        g.drawString("Score: " + score,getX(),getY()+15);
    }

    private void drawWelcome(Graphics g) {
        if(viewable.getGameState() == GameState.WELCOME) {
            g.setColor(new Color(255,255,255,60));
            g.fillRect(getX(),getY(),getWidth(),getHeight());
            g.setColor(Color.BLACK);
            g.setFont(welcomeFont);
            GraphicHelperMethods.drawCenteredString(
                    g,"WELCOME",
                    getX(),getY(),getWidth()-getX(),getHeight()-getY()-300);
            g.setFont(subFont);
            GraphicHelperMethods.drawCenteredString(
                    g,"arrow keys to move",
                    getX(),getY(),getWidth()-getX(),getHeight()+75);
            GraphicHelperMethods.drawCenteredString(
                    g,"'esc' to pause",
                    getX(),getY(),getWidth()-getX(),getHeight()+150);
            GraphicHelperMethods.drawCenteredString(
                    g,"'space' to play",
                    getX(),getY(),getWidth()-getX(),getHeight()+225);
        }
    }

    private void drawPause(Graphics g) {
        if(viewable.getGameState() == GameState.PAUSED) {
            g.setColor(new Color(255,255,255,60));
            g.fillRect(getX(),getY(),getWidth(),getHeight());
            g.setColor(Color.BLACK);
            g.setFont(welcomeFont);
            GraphicHelperMethods.drawCenteredString(
                    g,"PAUSED",
                    getX(),getY(),getWidth()-getX(),getHeight()-getY()-300);
            GraphicHelperMethods.drawCenteredString(
                    g,"SCORE: " + score,
                    getX(),getY(),getWidth()-getX(),getHeight()-getY()-100);
            g.setFont(subFont);
            GraphicHelperMethods.drawCenteredString(
                    g,"'esc' to resume'",
                    getX(),getY(),getWidth()-getX(),getHeight()+75);
            GraphicHelperMethods.drawCenteredString(
                    g,"'r' to restart",
                    getX(),getY(),getWidth()-getX(),getHeight()+150);
        }
    }

    private void drawGameOver(Graphics g) {
        if(viewable.getGameState() == GameState.GAME_OVER) {
            g.setColor(new Color(255,255,255,60));
            g.fillRect(getX(),getY(),getWidth(),getHeight());
            g.setColor(Color.BLACK);
            g.setFont(welcomeFont);
            GraphicHelperMethods.drawCenteredString(
                    g,"GAME OVER!",
                    getX(),getY(),getWidth()-getX(),getHeight()-getY()-300);
            GraphicHelperMethods.drawCenteredString(
                    g,"SCORE: " + score,
                    getX(),getY(),getWidth()-getX(),getHeight()-getY()-100);
            g.setFont(subFont);
            GraphicHelperMethods.drawCenteredString(
                    g,"u stink",
                    getX(),getY(),getWidth()-getX(),getHeight()+75);
            GraphicHelperMethods.drawCenteredString(
                    g,"'space' to play again",
                    getX(),getY(),getWidth()-getX(),getHeight()+150);
        }
    }

    /**
     * Loops through and calculates values for each Tile based on integers that represent the Board.
     * If a tile has snake, give it color for snake.
     * If a tile has fruit, give it color for fruit.
     * If a tile has grass, give it one of two grass colors depending on modulo
     * @param g Graphics to paint on
     * @param xBoard x value for Board
     * @param yBoard y value for Board
     * @param boardWidth width of Board
     * @param boardHeight height of Board
     * @param iterable iterable to loop through each Tile
     */
    private void drawBoard(Graphics g, int xBoard, int yBoard, int boardWidth, int boardHeight, Iterable<CoordinateItem<Tile>> iterable) {
        // Draw the board
        for (CoordinateItem<Tile> tile : iterable) {

            // Calculations for tiles
            int x = xBoard + tile.coordinate.col * boardWidth / viewable.getCols();
            int y = yBoard + tile.coordinate.row * boardHeight / viewable.getRows();
            int nextX = xBoard + (tile.coordinate.col + 1) * boardWidth / viewable.getCols();
            int nextY = yBoard + (tile.coordinate.row + 1) * boardHeight / viewable.getRows();
            int tileWidth = nextX - x;
            int tileHeight = nextY - y;

            // Colors
            Color g1 = new Color(0,150,0,200); // Grass 1
            Color g2 = new Color(0,150,0,155); // Grass 2
            Color snakeColor = new Color(0,100,255,180); // Snake
            Color fruitColor = Color.RED; // Fruit

            // Coloring and drawing
            Color color = null;
            if(tile.item.number == 0) {
                if((tile.coordinate.row + tile.coordinate.col) % 2 == 0) {
                    color = g1;
                } else color = g2;
            } else if(tile.item.number == 1) {
                color = snakeColor;
            } else if (tile.item.number == 2) {
                color = fruitColor;
                drawFruit(g,x,y,tileWidth,tileHeight,color);
            } if(tile.item.number != 2) {
                drawTile(g,x,y,tileWidth,tileHeight,color);
            }
        }
    }

    private void drawSnakeBoard(Graphics g,int x,int y,int width,int height,Iterable<CoordinateItem<Tile>> iterable) {
        drawBoard(g,x,y,width,height,iterable);
    }

    @Override
    public Dimension getPreferredSize() {
        int width = 590;
        int height = 590;
        return new Dimension(width, height);
    }

}
