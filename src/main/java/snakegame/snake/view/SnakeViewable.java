package snakegame.snake.view;

import snakegame.grid.CoordinateItem;
import snakegame.snake.model.GameState;
import snakegame.snake.model.Tile;

public interface SnakeViewable {

    /** @return number of rows in grid */
    int getRows();

    /** @return number of columns in grid */
    int getCols();

    /** @return score, based on number of fruits collected */
    int getScore();

    /** @return iterable of type CoordinateItem<Tile> representing the board */
    Iterable<CoordinateItem<Tile>> boardCords();

    /** @return iterable of type CoordinateItem<Tile> representing the snake */
    Iterable<CoordinateItem<Tile>> snakeCords();

    /** @return iterable of type CoordinateItem<Tile> representing the fruit */
    Iterable<CoordinateItem<Tile>> fruitCords();

    /** @return current game state */
    GameState getGameState();



}
