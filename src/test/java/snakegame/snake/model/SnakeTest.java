package snakegame.snake.model;

import org.junit.jupiter.api.Test;
import snakegame.grid.Coordinate;
import snakegame.grid.CoordinateItem;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SnakeTest {

    @Test
    void testUpdateSnakeHead() {
        Snake snake = new Snake(1);
        snake.updateSnake(1,0,false);
        Coordinate expected = new Coordinate(5,4);
        assertEquals(snake.snake.get(0).coordinate,expected);
    }

    @Test
    void testUpdateSnakeBody() {
        Snake snake = new Snake(1);
        snake.updateSnake(0,1,false);
        int c = 8;
        for(CoordinateItem<Tile> tile : snake) {
            assertEquals(new Coordinate(4,c),tile.coordinate);
            c--;
        }
    }

    @Test
    void snakeGrowth() {
        Snake snake = new Snake(1);
        assertEquals(4,snake.snake.size());
        snake.updateSnake(0,1,true);
        int c = 8;
        for(CoordinateItem<Tile> tile : snake) {
            assertEquals(new Coordinate(4,c),tile.coordinate);
            c--;
        }
        assertEquals(5,snake.snake.size());
    }

}
