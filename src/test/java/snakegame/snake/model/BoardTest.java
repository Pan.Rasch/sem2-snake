package snakegame.snake.model;

import org.junit.jupiter.api.Test;
import snakegame.grid.Coordinate;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class BoardTest {

    @Test
    void toCharArrayTest() {
        Board board = new Board(5,5,null);
        board.set(new Coordinate(2,1),new Tile(null,1));
        board.set(new Coordinate(2,2),new Tile(null,2));
        board.set(new Coordinate(2,3),new Tile(null,3));

        char[][] actualArray = board.toCharArray2d();
        String actual = board.charArray2dToString(actualArray);
        String expected =
                        """
                        -----
                        -----
                        -ooo-
                        -----
                        -----
                        """;

        assertEquals(expected,actual);
    }


}
