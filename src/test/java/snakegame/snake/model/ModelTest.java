package snakegame.snake.model;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.provider.EnumSource;
import snakegame.grid.Coordinate;
import snakegame.grid.CoordinateItem;

import static org.junit.jupiter.api.Assertions.*;

public class ModelTest {

    @Test
    void moveSnakeTest() {
        Snake snake = new Snake(1);
        Model model = new Model(snake,new Coordinate(0,0));
        Board board = new Board(1,1,null); // To access boardToCharArray
        model.clockTick();
        char[][] actualArray = model.boardToCharArray();
        String actual = board.charArray2dToString(actualArray);
        String expected =
                        """
                        ----------
                        ----------
                        ----------
                        ----------
                        ----ooooo-
                        """;
        assertEquals(expected,actual);
    }

    @Test
    void moveSnakeOutOfBounds() {
        Board board = new Board(1,1,null); // For toString method
        Snake snake = new Snake(1);
        Model model = new Model(snake,new Coordinate(0,0));
        for(int i = 0; i < 20; i++) {
            model.clockTick();
        }
        char[][] actualArray = model.boardToCharArray();
        String actual = board.charArray2dToString(actualArray);
        String expected =
                """
                ----------
                ----------
                ----------
                ----------
                -----ooooo
                """;
        assertEquals(expected,actual);
    }

    @Test
    void moveSnakeEastNorthWest() {
        Board board = new Board(1,1,null); // For toString method
        Snake snake = new Snake(1);
        Model model = new Model(snake,new Coordinate(0,0));
        model.clockTick();
        model.changeDirection(Direction.NORTH);
        model.clockTick();
        model.changeDirection(Direction.WEST);
        model.clockTick();
        char[][] actualArray = model.boardToCharArray();
        String actual = board.charArray2dToString(actualArray);
        String expected =
                """
                ----------
                ----------
                ----------
                -------oo-
                ------ooo-
                """;
        assertEquals(expected,actual);
    }

    @Test
    void moveSnakeOnSnake() {
        Board board = new Board(1,1,null); // For toString method
        Snake snake = new Snake(1);
        Model model = new Model(snake,new Coordinate(0,0));
        model.clockTick();
        model.changeDirection(Direction.NORTH);
        model.clockTick();
        model.changeDirection(Direction.WEST);
        model.clockTick();
        model.changeDirection(Direction.SOUTH);

        // Uncomment and set moveSnake() visibility to default
        // assertFalse(model.moveSnake());
    }

    @Test
    void outOfBoundsGameOver() {
        Snake snake = new Snake(1);
        Model model = new Model(snake,new Coordinate(0,0));
        for(int i = 0; i < 10; i++) {
            model.clockTick();
        }
        assertEquals(GameState.GAME_OVER,model.getGameState());
    }

    @Test
    void eatYourSelfGameOver() {
        Snake snake = new Snake(1);
        Model model = new Model(snake,new Coordinate(0,0));
        model.clockTick();
        model.changeDirection(Direction.NORTH);
        model.clockTick();
        model.changeDirection(Direction.WEST);
        model.clockTick();
        model.changeDirection(Direction.SOUTH);
        model.clockTick();
        assertEquals(GameState.GAME_OVER,model.getGameState());
    }

    @Test
    void eatFruitOnTick() {
        Board board = new Board(0,0,null);
        Snake snake = new Snake(1);
        Model model = new Model(snake,new Coordinate(4,9));
        model.clockTick();
        model.clockTick();
        model.changeDirection(Direction.NORTH);
        model.clockTick();
        model.clockTick();
        model.changeDirection(Direction.WEST);
        model.clockTick();
        model.clockTick();
        model.clockTick();

        char[][] actualArray = model.boardToCharArray();
        String actual = board.charArray2dToString(actualArray);
        String expected =
                """
                ----------
                ----------
                ------oooo
                ---------o
                ---------o
                """;
        assertEquals(expected,actual);
        assertEquals(6,snake.snake.size());
    }

}
