package snakegame.grid;

import org.junit.jupiter.api.Test;

import java.util.Objects;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertFalse;

/**
 * Testing the class CoordinateItem
 */
public class CoordinateItemTest {
    @Test
    void coordinateItemSanityTest() {
        String item = "Test";
        Coordinate coordinate = new Coordinate(5, 3);
        CoordinateItem<String> coordinateItem = new CoordinateItem<>(coordinate, item);

        assertEquals(coordinate, coordinateItem.coordinate);
        assertEquals(item, coordinateItem.item);
    }

    @Test
    void coordinateItemToStringTest() {
        String item = "Test";
        Coordinate coordinate = new Coordinate(5, 3);
        CoordinateItem<String> coordinateItem = new CoordinateItem<>(coordinate, item);

        assertEquals("{ coordinate='{ row='5', col='3' }', item='Test' }", coordinateItem.toString());
    }

    @Test
    void coordinateItemEqualityAndHashCodeTest() {
        String item = "Test";
        Coordinate coordinate = new Coordinate(5, 3);
        CoordinateItem<String> coordinateItem = new CoordinateItem<>(coordinate, item);

        String item2 = "Test";
        Coordinate coordinate2 = new Coordinate(5, 3);
        CoordinateItem<String> coordinateItem2 = new CoordinateItem<>(coordinate2, item2);

        assertTrue(coordinateItem2.equals(coordinateItem));
        assertTrue(coordinateItem.equals(coordinateItem2));
        assertTrue(Objects.equals(coordinateItem, coordinateItem2));
        assertTrue(coordinateItem.hashCode() == coordinateItem2.hashCode());
    }
}
