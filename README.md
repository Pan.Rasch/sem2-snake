# How to Play! (for idiots)
### GIT, Java, Maven ###
- Download and install [GIT](https://git-scm.com/downloads) if not already installed (type 'git' in terminal).
- Download and install [Java](https://jdk.java.net/17/) and [Maven](https://maven.apache.org/install.html) for your OS. [Guide for Mac](https://codippa.com/install-openjdk16-macos/).

- Download and install IDE of choice, e.g: 
[VSCode](https://code.visualstudio.com/download),
[IntelliJ](https://www.jetbrains.com/idea/download/#section=mac),
[Eclipse](https://www.eclipse.org/downloads/).
---
### Cloning the project using GIT
1. Navigate to [this video](https://www.youtube.com/watch?v=lw3Vz6WsomM) and follow instructions to set up GIT.
2. Navigate [here](https://git.app.uib.no/Pan.Rasch/sem2-snake). Press 'clone' and copy SSH url.
3. Open your terminal, navigate to folder of choice using cd command.
4. Enter 'git clone' followed by the url in your clipboard.
---
### Launching the game (almost there)
1. In your IDE, open the project you have just cloned.
2. Locate and press 'Run' button. The project will compile and launch.
3. You're done!
---
## CONTROLS
- 'Space' to start.
- Arrow keys to move.
- 'Escape' to pause while in-game. While paused, 'escape' to resume, 'r' to restart.
- 'Space' to play again whenever you lose. 
---
made by me

(c) Pan Rasch 2022.

